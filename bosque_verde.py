from tkinter import *
import os
import random

ventana_principal = Tk()
ventana_principal.title("Pokemon New")
ventana_principal.minsize(850, 550)
ventana_principal.resizable(width=False, height=False)
ventana_principal.config(bg='black')
canvas_bosque = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_bosque.config(bg="black")
canvas_bosque.pack()
rec = canvas_bosque.create_rectangle(0, 0, 850, 60)
rec1 = canvas_bosque.create_rectangle(0, 0, 40, 550)
rec2 = canvas_bosque.create_rectangle(0, 535, 715, 550)
rec3 = canvas_bosque.create_rectangle(820, 0, 850, 550)
rec4 = canvas_bosque.create_rectangle(525, 350, 625, 550)
rec5 = canvas_bosque.create_rectangle(720, 310, 850, 440)
rec6 = canvas_bosque.create_rectangle(510, 113, 750, 250)
rec7 = canvas_bosque.create_rectangle(380, 0, 430, 400)
rec8 = canvas_bosque.create_rectangle(255, 172, 300, 470)
rec9 = canvas_bosque.create_rectangle(125, 0, 170, 480)
rec10 = canvas_bosque.create_rectangle(285, 430, 330, 550)
rec11 = canvas_bosque.create_rectangle(330, 467, 525, 550)
rec12 = canvas_bosque.create_rectangle(0, 0, 170, 125)
cuadrante1 = canvas_bosque.create_rectangle(40, 220, 125, 512)
cuadrante2 = canvas_bosque.create_rectangle(170, 200, 255, 473)
cuadrante3 = canvas_bosque.create_rectangle(300, 200, 380, 397)
cuadrante4 = canvas_bosque.create_rectangle(430, 200, 510, 360)
cuadrante5 = canvas_bosque.create_rectangle(528, 260, 590, 350)


def load_img(name):
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img


global ash, numero_imagen_ash, numero_imagen_pika
numero_imagen_ash = 0
numero_imagen_pika = 0


# transicion_imagenes_ash
# Entradas: lista de imagenes
# Salidas: siguiente imagen(transicion)
def movimiento_ash(movimientos, fondo):
    global numero_imagen_ash
    numero_imagen_ash += 1
    if numero_imagen_ash == len(movimientos):
        numero_imagen_ash = 0
    fondo.itemconfig(ash, image=movimientos[numero_imagen_ash])


def movimiento_pika(movimientos, fondo):
    global numero_imagen_pika
    numero_imagen_pika += 1
    if numero_imagen_pika == len(movimientos):
        numero_imagen_pika = 0
    fondo.itemconfig(pika, image=movimientos[numero_imagen_pika])


bosque_verde1 = load_img("bosque_verde.png")
bosque_verde = canvas_bosque.create_image(0, 0, anchor=NW, image=bosque_verde1)
pika1 = load_img("pikachu_e_1.png")
pika = canvas_bosque.create_image(685, 413, anchor=CENTER, image=pika1)
ash1 = load_img("ash_e_1.png")
ash = canvas_bosque.create_image(645, 408, anchor=CENTER, image=ash1)
abra = load_img("Abra.png")
butterfree = load_img("Butterfree.png")
beedrill = load_img("Beedrill.png")
bellsprout = load_img("Bellsprout.png")
caterpie = load_img("Caterpie.png")
cubone = load_img("Cubone.png")
exeggcute = load_img("Exeggcute.png")
gastly = load_img("Gastly.png")
growlithe = load_img("Growlithe.png")
jigglypuff = load_img("Jigglypuff.png")
nidoran_hembra = load_img("NidoranH.png")
nidoran_macho = load_img("NidoranM.png")
pidgey = load_img("Pidgey.png")
poliwhirl = load_img("Poliwhirl.png")
poliwrath = load_img("Poliwrath.png")
seel = load_img("Seel.png")

# Bbox de los objetos
rec_bbox = canvas_bosque.bbox(rec)
rec1_bbox = canvas_bosque.bbox(rec1)
rec2_bbox = canvas_bosque.bbox(rec2)
rec3_bbox = canvas_bosque.bbox(rec3)
rec4_bbox = canvas_bosque.bbox(rec4)
rec5_bbox = canvas_bosque.bbox(rec5)
rec6_bbox = canvas_bosque.bbox(rec6)
rec7_bbox = canvas_bosque.bbox(rec7)
rec8_bbox = canvas_bosque.bbox(rec8)
rec9_bbox = canvas_bosque.bbox(rec9)
rec10_bbox = canvas_bosque.bbox(rec10)
rec11_bbox = canvas_bosque.bbox(rec11)
rec12_bbox = canvas_bosque.bbox(rec12)

# lista de objetos
lista_objetos_bosque = [rec_bbox, rec1_bbox, rec2_bbox, rec3_bbox, rec4_bbox, rec5_bbox, rec6_bbox, rec7_bbox,
                        rec8_bbox, rec9_bbox, rec10_bbox, rec11_bbox, rec12_bbox]

# lista de pokemones
lista_pokemones = [abra, beedrill, bellsprout, butterfree, caterpie, cubone, exeggcute, gastly, growlithe, jigglypuff,
                   nidoran_hembra, nidoran_macho, pidgey, poliwhirl, poliwrath, seel]

# lista de cuadrantes en la hierba
lista_cuadrante1 = [[64, 247], [64, 275], [64, 303], [64, 331], [64, 380], [64, 415], [64, 457], [64, 450], [30, 450]]

lista_cuadrante2 = [[197, 240], [197, 296], [197, 345], [197, 373], [197, 401], [197, 429], [197, 420]]

lista_cuadrante3 = [[323, 219], [323, 254], [323, 323], [323, 324]]

lista_cuadrante4 = [[449, 226], [449, 261], [449, 303], [440, 220]]

mover_arriba1, mover_abajo1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                                           load_img("ash_e_3.png"), ], \
                                                          [load_img("ash_f_1.png"), load_img("ash_f_2.png"),
                                                           load_img("ash_f_3.png")], \
                                                          [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                                           load_img("ash_d_3.png"), ], \
                                                          [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                                           load_img("ash_i_2.png")]

mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                           load_img("pikachu_e_3.png"), ], \
                                                          [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                           load_img("pikachu_f_3.png")], \
                                                          [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                           load_img("pikachu_d_3.png"), ], \
                                                          [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                           load_img("pikachu_i_2.png")]


def aparicion_random(cont):
    if cont == 0:
        canvas_bosque.create_image(lista_cuadrante1[0], anchor=NW, image=lista_pokemones[0])
        canvas_bosque.create_image(lista_cuadrante2[1], anchor=NW, image=lista_pokemones[1])
        canvas_bosque.create_image(lista_cuadrante3[0], anchor=NW, image=lista_pokemones[2])
        canvas_bosque.create_image(lista_cuadrante4[1], anchor=NW, image=lista_pokemones[3])
    elif cont == 1:
        canvas_bosque.create_image(lista_cuadrante1[1], anchor=NW, image=lista_pokemones[4])
        canvas_bosque.create_image(lista_cuadrante2[2], anchor=NW, image=lista_pokemones[5])
        canvas_bosque.create_image(lista_cuadrante3[2], anchor=NW, image=lista_pokemones[6])
        canvas_bosque.create_image(lista_cuadrante4[0], anchor=NW, image=lista_pokemones[7])
    elif cont == 2:
        canvas_bosque.create_image(lista_cuadrante1[2], anchor=NW, image=lista_pokemones[8])
        canvas_bosque.create_image(lista_cuadrante2[3], anchor=NW, image=lista_pokemones[9])
        canvas_bosque.create_image(lista_cuadrante3[3], anchor=NW, image=lista_pokemones[10])
        canvas_bosque.create_image(lista_cuadrante4[2], anchor=NW, image=lista_pokemones[11])
    elif cont == 3:
        canvas_bosque.create_image(lista_cuadrante1[3], anchor=NW, image=lista_pokemones[12])
        canvas_bosque.create_image(lista_cuadrante2[4], anchor=NW, image=lista_pokemones[13])
        canvas_bosque.create_image(lista_cuadrante3[0], anchor=NW, image=lista_pokemones[14])
        canvas_bosque.create_image(lista_cuadrante4[2], anchor=NW, image=lista_pokemones[15])
    elif cont == 4:
        canvas_bosque.create_image(lista_cuadrante1[4], anchor=NW, image=lista_pokemones[11])
        canvas_bosque.create_image(lista_cuadrante2[5], anchor=NW, image=lista_pokemones[5])
        canvas_bosque.create_image(lista_cuadrante3[1], anchor=NW, image=lista_pokemones[13])
        canvas_bosque.create_image(lista_cuadrante4[3], anchor=NW, image=lista_pokemones[6])
    elif cont == 5:
        canvas_bosque.create_image(lista_cuadrante1[5], anchor=NW, image=lista_pokemones[3])
        canvas_bosque.create_image(lista_cuadrante2[6], anchor=NW, image=lista_pokemones[10])
        canvas_bosque.create_image(lista_cuadrante3[2], anchor=NW, image=lista_pokemones[11])
        canvas_bosque.create_image(lista_cuadrante4[0], anchor=NW, image=lista_pokemones[7])
    elif cont == 6:
        canvas_bosque.create_image(lista_cuadrante1[6], anchor=NW, image=lista_pokemones[3])
        canvas_bosque.create_image(lista_cuadrante2[6], anchor=NW, image=lista_pokemones[2])
        canvas_bosque.create_image(lista_cuadrante3[3], anchor=NW, image=lista_pokemones[5])
        canvas_bosque.create_image(lista_cuadrante4[3], anchor=NW, image=lista_pokemones[4])
    elif cont == 7:
        canvas_bosque.create_image(lista_cuadrante1[7], anchor=NW, image=lista_pokemones[15])
        canvas_bosque.create_image(lista_cuadrante2[2], anchor=NW, image=lista_pokemones[7])
        canvas_bosque.create_image(lista_cuadrante3[1], anchor=NW, image=lista_pokemones[8])
        canvas_bosque.create_image(lista_cuadrante4[1], anchor=NW, image=lista_pokemones[14])
    elif cont == 8:
        canvas_bosque.create_image(lista_cuadrante1[8], anchor=NW, image=lista_pokemones[11])
        canvas_bosque.create_image(lista_cuadrante2[0], anchor=NW, image=lista_pokemones[12])
        canvas_bosque.create_image(lista_cuadrante3[1], anchor=NW, image=lista_pokemones[9])
        canvas_bosque.create_image(lista_cuadrante4[3], anchor=NW, image=lista_pokemones[3])



def calcular_coords_x(coordenada_x1, coordenada_x2, lista_coordx):
    if coordenada_x1 > coordenada_x2:
        return lista_coordx
    else:
        lista_coordx.append(coordenada_x1)
        return calcular_coords_x(coordenada_x1 + 1, coordenada_x2, lista_coordx)


def calcular_coords_y(coordenada_y1, coordenada_y2, lista_coordy):
    if coordenada_y1 > coordenada_y2:
        return lista_coordy
    else:
        lista_coordy.append(coordenada_y1)
        return calcular_coords_y(coordenada_y1 + 1, coordenada_y2, lista_coordy)


def colisiones_arriba1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_bosque.coords(pika)[1] - 18 < lista_objetos[0][3]:
        if canvas_bosque.coords(pika)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_arriba1(lista_objetos[1:])
    else:
        return colisiones_arriba1(lista_objetos[1:])


def colisiones_abajo1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_bosque.coords(pika)[1] + 18 < lista_objetos[0][3]:
        if canvas_bosque.coords(pika)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_abajo1(lista_objetos[1:])
    else:
        return colisiones_abajo1(lista_objetos[1:])


def colisiones_derecha1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_bosque.coords(pika)[0] + 18 < lista_objetos[0][2]:
        if canvas_bosque.coords(pika)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_derecha1(lista_objetos[1:])
    else:
        return colisiones_derecha1(lista_objetos[1:])


def colisiones_izquierda1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_bosque.coords(pika)[0] - 18 < lista_objetos[0][2]:
        if canvas_bosque.coords(pika)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_izquierda1(lista_objetos[1:])
    else:
        return colisiones_izquierda1(lista_objetos[1:])


def colisiones_arriba(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_bosque.coords(ash)[1] - 18 < lista_objetos[0][3]:
        if canvas_bosque.coords(ash)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_arriba(lista_objetos[1:])
    else:
        return colisiones_arriba(lista_objetos[1:])


def colisiones_abajo(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_bosque.coords(ash)[1] + 18 < lista_objetos[0][3]:
        if canvas_bosque.coords(ash)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_abajo(lista_objetos[1:])
    else:
        return colisiones_abajo(lista_objetos[1:])


def colisiones_derecha(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_bosque.coords(ash)[0] + 18 < lista_objetos[0][2]:
        if canvas_bosque.coords(ash)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_derecha(lista_objetos[1:])
    else:
        return colisiones_derecha(lista_objetos[1:])


def colisiones_izquierda(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_bosque.coords(ash)[0] - 18 < lista_objetos[0][2]:
        if canvas_bosque.coords(ash)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_izquierda(lista_objetos[1:])
    else:
        return colisiones_izquierda(lista_objetos[1:])


def mover_arriba(event):
    if colisiones_arriba(lista_objetos_bosque) == False or colisiones_arriba1(lista_objetos_bosque) == False:
        movimiento_ash(mover_arriba1, canvas_bosque)
        canvas_bosque.move(ash, 0, 0)
        movimiento_pika(mover_arriba2, canvas_bosque)
        canvas_bosque.move(pika, 0, 0)
    else:
        movimiento_ash(mover_arriba1, canvas_bosque)
        canvas_bosque.move(ash, 0, -7)
        movimiento_pika(mover_arriba2, canvas_bosque)
        canvas_bosque.move(pika, 0, -7)


def mover_abajo(event):
    if colisiones_abajo(lista_objetos_bosque) == False or colisiones_abajo1(lista_objetos_bosque) == False:
        movimiento_ash(mover_abajo1, canvas_bosque)
        canvas_bosque.move(ash, 0, 0)
        movimiento_pika(mover_abajo2, canvas_bosque)
        canvas_bosque.move(pika, 0, 0)
    else:
        movimiento_ash(mover_abajo1, canvas_bosque)
        canvas_bosque.move(ash, 0, 7)
        movimiento_pika(mover_abajo2, canvas_bosque)
        canvas_bosque.move(pika, 0, 7)


def mover_derecha(event):
    if colisiones_derecha(lista_objetos_bosque) == False or colisiones_derecha1(lista_objetos_bosque) == False:
        movimiento_ash(mover_derecha1, canvas_bosque)
        canvas_bosque.move(ash, 0, 0)
        movimiento_pika(mover_derecha2, canvas_bosque)
        canvas_bosque.move(pika, 0, 0)
        return pos()
    else:
        movimiento_ash(mover_derecha1, canvas_bosque)
        canvas_bosque.move(ash, 7, 0)
        movimiento_pika(mover_derecha2, canvas_bosque)
        canvas_bosque.move(pika, 7, 0)
        return pos()


def mover_izquierda(event):
    if colisiones_izquierda(lista_objetos_bosque) == False or colisiones_izquierda1(lista_objetos_bosque) == False:
        movimiento_ash(mover_izq1, canvas_bosque)
        canvas_bosque.move(ash, 0, 0)
        movimiento_pika(mover_izq2, canvas_bosque)
        canvas_bosque.move(pika, 0, 0)
    else:
        movimiento_ash(mover_izq1, canvas_bosque)
        canvas_bosque.move(ash, -7, 0)
        movimiento_pika(mover_izq2, canvas_bosque)
        canvas_bosque.move(pika, -7, 0)


def lista_pokemon():
    ventana_pokemon = Tk()
    ventana_pokemon.title("lista de pokemon ")
    ventana_pokemon.minsize(300, 300)
    ventana_pokemon.resizable(width=False, height=False)
    ventana_pokemon.config(bg='white')
    canvas_lista_pokemon = Canvas(ventana_pokemon, width=300, height=300, bd=0, highlightthickness=0)
    canvas_lista_pokemon.config(bg="black")
    canvas_lista_pokemon.pack()


def menu(event):
    ventana_menu = Tk()
    ventana_menu.title("Menu del jugador")
    ventana_menu.minsize(300, 300)
    ventana_menu.resizable(width=False, height=False)
    ventana_menu.config(bg='white')
    Button(ventana_menu, width=15, height=5, command=exit, text="Salir", bg="white", fg="black").place(x=25, y=200)
    Button(ventana_menu, width=15, height=5, command=exit, text="Guardar", bg="white", fg="black").place(x=25, y=100)
    Button(ventana_menu, width=15, height=5, command=lista_pokemon, text="Pokemon", bg="white", fg="black").place(x=25,
                                                                                                                  y=5)


ventana_principal.bind("<space>", menu)
ventana_principal.bind("<Up>", mover_arriba)
ventana_principal.bind("<Left>", mover_izquierda)
ventana_principal.bind("<Right>", mover_derecha)
ventana_principal.bind("<Down>", mover_abajo)
aparicion_random()
ventana_principal.update()
ventana_principal.mainloop()
