from tkinter import *
import os

ventana_principal = Tk()
ventana_principal.title("Pokemon New")
ventana_principal.minsize(850, 550)
ventana_principal.resizable(width=False, height=False)
ventana_principal.config(bg='black')

canvas_plateda = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_plateda.config(bg="black")
canvas_plateda.pack()


def load_img(name):
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img


global ash, numero_imagen_ash, numero_imagen_pika

ciudad_plateada1 = load_img("ciudad_plateada.png")
paleta = canvas_plateda.create_image(0, 0, anchor=NW, image=ciudad_plateada1)
pika1 = load_img("pikachu_e_1.png")
pika = canvas_plateda.create_image(370, 107, anchor=NW, image=pika1)
ash1 = load_img("ash_e_1.png")
ash = canvas_plateda.create_image(334, 100, anchor=NW, image=ash1)

numero_imagen_ash = 0
numero_imagen_pika = 0


# transicion_imagenes_ash
# Entradas: lista de imagenes
# Salidas: siguiente imagen(transicion)
def movimiento_ash(movimientos, fondo):
    global numero_imagen_ash
    numero_imagen_ash += 1
    if numero_imagen_ash == len(movimientos):
        numero_imagen_ash = 0
    fondo.itemconfig(ash, image=movimientos[numero_imagen_ash])


def movimiento_pika(movimientos, fondo):
    global numero_imagen_pika
    numero_imagen_pika += 1
    if numero_imagen_pika == len(movimientos):
        numero_imagen_pika = 0
    fondo.itemconfig(pika, image=movimientos[numero_imagen_pika])


mover_arriba1, mover_abajo1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                                           load_img("ash_e_3.png"), ], \
                                                          [load_img("ash_f_1.png"), load_img("ash_f_2.png"),
                                                           load_img("ash_f_3.png")], \
                                                          [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                                           load_img("ash_d_3.png"), ], \
                                                          [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                                           load_img("ash_i_2.png")]

mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                           load_img("pikachu_e_3.png"), ], \
                                                          [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                           load_img("pikachu_f_3.png")], \
                                                          [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                           load_img("pikachu_d_3.png"), ], \
                                                          [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                           load_img("pikachu_i_2.png")]


def mover_arriba(event):
    movimiento_ash(mover_arriba1, canvas_plateda)
    canvas_plateda.move(ash, 0, -7)
    movimiento_pika(mover_arriba2, canvas_plateda)
    canvas_plateda.move(pika, 0, -7)


def mover_abajo(event):
    movimiento_ash(mover_abajo1, canvas_plateda)
    canvas_plateda.move(ash, 0, 7)
    movimiento_pika(mover_abajo2, canvas_plateda)
    canvas_plateda.move(pika, 0, 7)


def mover_derecha(event):
    movimiento_ash(mover_derecha1, canvas_plateda)
    canvas_plateda.move(ash, 7, 0)
    movimiento_pika(mover_derecha2, canvas_plateda)
    canvas_plateda.move(pika, 7, 0)


def mover_izquierda(event):
    movimiento_ash(mover_izq1, canvas_plateda)
    canvas_plateda.move(ash, -7, 0)
    movimiento_pika(mover_izq2, canvas_plateda)
    canvas_plateda.move(pika, -7, 0)


ventana_principal.bind("<Up>", mover_arriba)
ventana_principal.bind("<Left>", mover_izquierda)
ventana_principal.bind("<Right>", mover_derecha)
ventana_principal.bind("<Down>", mover_abajo)

ventana_principal.update()
ventana_principal.mainloop()