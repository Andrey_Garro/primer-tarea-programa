from tkinter import *
import os

# ventana principal
ventana_principal = Tk()
ventana_principal.title("Pokemon New")
ventana_principal.minsize(850, 550)
ventana_principal.resizable(width=False, height=False)
ventana_principal.config(bg='black')

# canvas sobre el cual va la interfaz grafica
canvas_principal = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_principal.config(bg="black")
canvas_principal.pack()
canvas_paleta = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_paleta.config(bg="pink")


# load_img
# Entradas: nombre de la imagen que se desea llamar
# Salidas: la imagen formato .png que fue  llamada
def load_img(name):
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img


# lista de imagenes
lista_imagenes = [load_img("intro_oak1.png"), load_img("intro_oak2.png"), load_img("intro_oak3.png"),
                  load_img("intro_oak4.png"),
                  load_img("intro_oak5.png"), load_img("intro_oak6.png"), load_img("intro_oak7.png"),
                  load_img("intro_oak8.png"), load_img("intro_oak9.png"), load_img("intro_oak10.png"),
                  load_img("intro_oak11.png"), load_img("intro_oak12.png"), load_img("intro_oak13.png"),
                  load_img("intro_oak14.png"), load_img("intro_oak15.png"), load_img("intro_oak16.png"),
                  load_img("intro_oak17.png"),
                  load_img("intro_oak18.png"), load_img("intro_oak19.png"), load_img("intro_oak20.png"),
                  load_img("intro_oak21.png"),
                  load_img("intro_oak22.png"), load_img("interior_lab.png")]

# Carga la imagen de inicio
pic1 = load_img("pantalla_inicio.png")
canvas_principal.create_image(0, 0, anchor=NW, image=pic1, tag="inicio")


# press_enter
# Entradas: si se presiona la tecla "Enter"
# Salidas: devuelve a la funcion de las presentaciones del inicio
def press_enter(event):
    if "<Return>":
        return presentacion()


ventana_principal.bind("<Return>", press_enter)

# Asignacion de la variable para las transiciones de ash y pikachu
numero_imagen_ash = 0
numero_imagen_pika = 0


# Asignacion de la variable para mostrar el escenario de fondo
num_imagen = 0


# presentacion
# Entradas: ninguna
# Salidas: muestra la imagen correspondiente y captura el nombre del personaje, al final retorna la funcion laboratorio
def presentacion():
    global num_imagen, entrada, guarda_nombre, nombre
    if num_imagen == 14:
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
        entrada_nombre = StringVar()
        nombre = Entry(canvas_principal, textvariable=entrada_nombre)
        nombre.place(x=470, y=150)
        guarda_nombre = nombre.get()
    elif num_imagen == 15:
        nombre.destroy()
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
    elif num_imagen == 22:
        canvas_principal.destroy()
        return laboratorio()
    else:
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
    num_imagen += 1


# Creacion del canvas para el laboratorio, el laboratorio y el personaje
canvas_lab = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_lab.config(bg="white")
lab = load_img("interior_lab.png")
canvas_lab.create_image(0, 0, anchor=NW, image=lab, tag="labora")
ash1 = load_img("ash_e_1.png")
ash_lab = canvas_lab.create_image(445, 200, anchor=NW, image=ash1)
poke = load_img("pokeball_sprite.png")
pokeball = canvas_lab.create_image(560, 140, anchor=NW, image=poke)
estante_lab = load_img("estante_lab.png")
estante_lab1 = canvas_lab.create_image(652, 375, anchor=CENTER, image=estante_lab)
estante_lab2 = canvas_lab.create_image(86, 375, anchor=CENTER, image=estante_lab)
estante_lab3 = canvas_lab.create_image(764, 375, anchor=CENTER, image=estante_lab)
estante_lab4 = canvas_lab.create_image(198, 375, anchor=CENTER, image=estante_lab)


# laboratorio
# Entradas: ninguna
# Salidas: muestra el canvas del laboratorio junto con los objetos creados en el
def laboratorio():
    canvas_lab.pack()
    movimiento_lab()

# Bbox de los objetos
reclab = canvas_lab.create_rectangle(0, 0, 850, 130)
rec1lab = canvas_lab.create_rectangle(0, 290, 190, 130)
rec2lab = canvas_lab.create_rectangle(0, 450, 250, 340)
rec3lab = canvas_lab.create_rectangle(600, 450, 850, 340)
rec4lab = canvas_lab.create_rectangle(600, 275, 760, 205)
rec5lab = canvas_lab.create_rectangle(820, 0, 850, 550)
rec6lab = canvas_lab.create_rectangle(0, 0, 30, 550)
rec7lab = canvas_lab.create_rectangle(0, 520, 375, 850)
rec8lab = canvas_lab.create_rectangle(475, 520, 850, 850)
rec9lab = canvas_lab.create_rectangle(433, 130, 473, 190)
reclab_bbox = canvas_lab.bbox(reclab)
rec1lab_bbox = canvas_lab.bbox(rec1lab)
rec2lab_bbox = canvas_lab.bbox(rec2lab)
rec3lab_bbox = canvas_lab.bbox(rec3lab)
rec4lab_bbox = canvas_lab.bbox(rec4lab)
rec5lab_bbox = canvas_lab.bbox(rec5lab)
rec6lab_bbox = canvas_lab.bbox(rec6lab)
rec7lab_bbox = canvas_lab.bbox(rec7lab)
rec8lab_bbox = canvas_lab.bbox(rec8lab)
rec9lab_bbox = canvas_lab.bbox(rec9lab)
lista_objetos_lab = [reclab_bbox, rec1lab_bbox, rec2lab_bbox, rec3lab_bbox, rec4lab_bbox, rec5lab_bbox,
                         rec6lab_bbox, rec7lab_bbox, rec8lab_bbox,
                         rec9lab_bbox]




# elimina_sprite
# Entradas: ninguna
# Salidas: elimina el sprite de la pokebola cuando el personaje se ubica frente a ella y crea el pikachu a la par
def elimina_sprite():
    canvas_lab.delete(pokeball)


pueblo_paleta1 = load_img("paleta.png")
canvas_paleta.create_image(0, 0, anchor=NW, image=pueblo_paleta1)
pika1 = load_img("pikachu_e_1.png")
pika_paleta = canvas_paleta.create_image(535, 447, anchor=CENTER, image=pika1)
ash_paleta = canvas_paleta.create_image(565, 440, anchor=CENTER, image=ash1)
casa_sprite1 = load_img("casa_sprite.png")
imagencasa = canvas_paleta.create_image(132, 6, anchor=NW, image=casa_sprite1)
imagencasa2 = canvas_paleta.create_image(435, -8, anchor=NW, image=casa_sprite1)
lab_sprite = load_img("lab_sprite.png")
lab_sprite1 = canvas_paleta.create_image(445, 228, anchor=NW, image=lab_sprite)
rec_paleta = canvas_paleta.create_rectangle(166, 65, 340, 220)
rec1_paleta = canvas_paleta.create_rectangle(470, 55, 640, 200)
rec2_paleta = canvas_paleta.create_rectangle(465, 270, 670, 440)
rec3_paleta = canvas_paleta.create_rectangle(155, 290, 370, 430)
rec4_paleta = canvas_paleta.create_rectangle(0, 0, 100, 550)
rec5_paleta = canvas_paleta.create_rectangle(740, 0, 850, 550)
rec6_paleta = canvas_paleta.create_rectangle(100, 535, 740, 550)
rec7_paleta = canvas_paleta.create_rectangle(0, 0, 650, 10)
rec_bbox_paleta = canvas_paleta.bbox(rec_paleta)
rec1_bbox_paleta = canvas_paleta.bbox(rec1_paleta)
rec2_bbox_paleta = canvas_paleta.bbox(rec2_paleta)
rec3_bbox_paleta = canvas_paleta.bbox(rec3_paleta)
rec4_bbox_paleta = canvas_paleta.bbox(rec4_paleta)
rec5_bbox_paleta = canvas_paleta.bbox(rec5_paleta)
rec6_bbox_paleta = canvas_paleta.bbox(rec6_paleta)
rec7_bbox_paleta = canvas_paleta.bbox(rec7_paleta)

lista_objetos_paleta = [rec_bbox_paleta, rec1_bbox_paleta, rec2_bbox_paleta, rec3_bbox_paleta, rec4_bbox_paleta, rec5_bbox_paleta, rec6_bbox_paleta, rec7_bbox_paleta]

def crear_pueblo_paleta():
    canvas_lab.destroy()
    canvas_paleta.pack()
    movimiento_paleta()


def calcular_coords_x(coordenada_x1, coordenada_x2, lista_coordx):
    if coordenada_x1 > coordenada_x2:
        return lista_coordx
    else:
        lista_coordx.append(coordenada_x1)
        return calcular_coords_x(coordenada_x1 + 1, coordenada_x2, lista_coordx)


def calcular_coords_y(coordenada_y1, coordenada_y2, lista_coordy):
    if coordenada_y1 > coordenada_y2:
        return lista_coordy
    else:
        lista_coordy.append(coordenada_y1)
        return calcular_coords_y(coordenada_y1 + 1, coordenada_y2, lista_coordy)


def movimiento_paleta():
    # movimiento_ash
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    def movimiento_ash_paleta(movimientos, fondo):
        global numero_imagen_ash
        numero_imagen_ash += 1
        if numero_imagen_ash == len(movimientos):
            numero_imagen_ash = 0
        fondo.itemconfig(ash_paleta, image=movimientos[numero_imagen_ash])

    def movimiento_pika_paleta(movimientos, fondo):
        global numero_imagen_pika
        numero_imagen_pika += 1
        if numero_imagen_pika == len(movimientos):
            numero_imagen_pika = 0
        fondo.itemconfig(pika_paleta, image=movimientos[numero_imagen_pika])

    def colisiones_arriba_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(pika_paleta)[1] - 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(pika_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_arriba_paleta_pika(lista_objetos[1:])

    def colisiones_abajo_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(pika_paleta)[1] + 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(pika_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_abajo_paleta_pika(lista_objetos[1:])

    def colisiones_derecha_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(pika_paleta)[0] + 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(pika_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_derecha_paleta_pika(lista_objetos[1:])

    def colisiones_izquierda_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(pika_paleta)[0] - 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(pika_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_izquierda_paleta_pika(lista_objetos[1:])

    def colisiones_arriba_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(ash_paleta)[1] - 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(ash_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_arriba_paleta_ash(lista_objetos[1:])

    def colisiones_abajo_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(ash_paleta)[1] + 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(ash_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_abajo_paleta_ash(lista_objetos[1:])

    def colisiones_derecha_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(ash_paleta)[0] + 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(ash_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_derecha_paleta_ash(lista_objetos[1:])

    def colisiones_izquierda_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(ash_paleta)[0] - 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(ash_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_izquierda_paleta_ash(lista_objetos[1:])

    def mover_arriba(event):
        if colisiones_arriba_paleta_ash(lista_objetos_paleta) == False or colisiones_arriba_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_arriba1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_arriba2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_arriba1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, -7)
            movimiento_pika_paleta(mover_arriba2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, -7)

    def mover_abajo(event):
        if colisiones_abajo_paleta_ash(lista_objetos_paleta) == False or colisiones_abajo_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_abajo1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_abajo2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_abajo1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 7)
            movimiento_pika_paleta(mover_abajo2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 7)

    def mover_derecha(event):
        if colisiones_derecha_paleta_ash(lista_objetos_paleta) == False or colisiones_derecha_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_derecha1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_derecha2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_derecha1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 7, 0)
            movimiento_pika_paleta(mover_derecha2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 7, 0)

    def mover_izquierda(event):
        if colisiones_izquierda_paleta_ash(lista_objetos_paleta) == False or colisiones_izquierda_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_izq1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_izq2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_izq1, canvas_paleta)
            canvas_paleta.move(ash_paleta, -7, 0)
            movimiento_pika_paleta(mover_izq2, canvas_paleta)
            canvas_paleta.move(pika_paleta, -7, 0)

    ventana_principal.bind("<space>", menu)
    ventana_principal.bind("<Up>", mover_arriba)
    ventana_principal.bind("<Left>", mover_izquierda)
    ventana_principal.bind("<Right>", mover_derecha)
    ventana_principal.bind("<Down>", mover_abajo)


# lista de los sprites de movimiento del personaje y pikachu
mover_arriba1, mover_abajo1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                                           load_img("ash_e_3.png"), ], \
                                                          [load_img("ash_f_1.png"), load_img("ash_f_2.png"),
                                                           load_img("ash_f_3.png")], \
                                                          [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                                           load_img("ash_d_3.png"), ], \
                                                          [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                                           load_img("ash_i_2.png")]

mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                           load_img("pikachu_e_3.png"), ], \
                                                          [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                           load_img("pikachu_f_3.png")], \
                                                          [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                           load_img("pikachu_d_3.png"), ], \
                                                          [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                           load_img("pikachu_i_2.png")]


def movimiento_lab():
    # movimiento_ash
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    def movimiento_ash_lab(movimientos, fondo):
        global numero_imagen_ash
        numero_imagen_ash += 1
        if numero_imagen_ash == len(movimientos):
            numero_imagen_ash = 0
        fondo.itemconfig(ash_lab, image=movimientos[numero_imagen_ash])

    def movimiento_pika_lab(movimientos, fondo):
        global numero_imagen_pika
        numero_imagen_pika += 1
        if numero_imagen_pika == len(movimientos):
            numero_imagen_pika = 0
        fondo.itemconfig(pika_lab, image=movimientos[numero_imagen_pika])

    def colisiones_arriba_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_lab.coords(ash_lab)[1] - 18 < lista_objetos[0][3]:
            if canvas_lab.coords(ash_lab)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_lab_ash(lista_objetos[1:])
        else:
            return colisiones_arriba_lab_ash(lista_objetos[1:])

    def colisiones_abajo_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_lab.coords(ash_lab)[1] + 18 < lista_objetos[0][3]:
            if canvas_lab.coords(ash_lab)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_lab_ash(lista_objetos[1:])
        else:
            return colisiones_abajo_lab_ash(lista_objetos[1:])

    def colisiones_derecha_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_lab.coords(ash_lab)[0] + 18 < lista_objetos[0][2]:
            if canvas_lab.coords(ash_lab)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_lab_ash(lista_objetos[1:])
        else:
            return colisiones_derecha_lab_ash(lista_objetos[1:])

    def colisiones_izquierda_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_lab.coords(ash_lab)[0] - 18 < lista_objetos[0][2]:
            if canvas_lab.coords(ash_lab)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_lab_ash(lista_objetos[1:])
        else:
            return colisiones_izquierda_lab_ash(lista_objetos[1:])

    # mover_arriba
    # Entradas: si se presiona la tecla: "Arrow-Up"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia arriba 7 pixeles
    def mover_arriba(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if colisiones_arriba_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_arriba1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_arriba1, canvas_lab)
            canvas_lab.move(ash_lab, 0, -7)

    # mover_abajo
    # Entradas: si se presiona la tecla: "Arrow-Down"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia abajo 7 pixeles
    def mover_abajo(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if canvas_lab.coords(ash_lab)[1] == 515:
            canvas_lab.destroy()
            return crear_pueblo_paleta()
        if colisiones_abajo_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_abajo1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_abajo1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 7)

    # mover_derecha
    # Entradas: si se presiona la tecla: "Arrow-Right"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia la derecha 7 pixeles
    def mover_derecha(event):
        if colisiones_derecha_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_derecha1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_derecha1, canvas_lab)
            canvas_lab.move(ash_lab, 7, 0)

    # mover_izquieda
    # Entradas: si se presiona la tecla: "Arrow-Left"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia la izquierda 7 pixeles
    def mover_izquierda(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if colisiones_izquierda_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_izq1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_izq1, canvas_lab)
            canvas_lab.move(ash_lab, -7, 0)

    ventana_principal.bind("<space>", menu)
    ventana_principal.bind("<Up>", mover_arriba)
    ventana_principal.bind("<Left>", mover_izquierda)
    ventana_principal.bind("<Right>", mover_derecha)
    ventana_principal.bind("<Down>", mover_abajo)


def lista_pokemon():
    ventana_pokemon = Tk()
    ventana_pokemon.title("lista de pokemon")
    ventana_pokemon.minsize(300, 300)
    ventana_pokemon.resizable(width=False, height=False)
    ventana_pokemon.config(bg='white')
    canvas_lista_pokemon = Canvas(ventana_pokemon, width=300, height=300, bd=0, highlightthickness=0)
    canvas_lista_pokemon.config(bg="black")
    canvas_lista_pokemon.pack()


def menu(event):
    ventana_menu = Tk()
    ventana_menu.title(nombre)
    ventana_menu.minsize(300, 300)
    ventana_menu.resizable(width=False, height=False)
    ventana_menu.config(bg='white')
    Button(ventana_menu, width=15, height=5, command=exit, text="Salir", bg="white", fg="black").place(x=25, y=200)
    Button(ventana_menu, width=15, height=5, command=exit, text="Guardar", bg="white", fg="black").place(x=25, y=100)
    Button(ventana_menu, width=15, height=5, command=lista_pokemon, text="Pokemon", bg="white", fg="black").place(x=25,
                                                                                                                  y=5)


ventana_principal.update()
ventana_principal.mainloop()
