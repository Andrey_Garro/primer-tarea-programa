from tkinter import *
import os

ventana_principal = Tk()
ventana_principal.title("Pokemon New")
ventana_principal.minsize(850, 550)
ventana_principal.resizable(width=False, height=False)
ventana_principal.config(bg='black')



def load_img(name):
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img


canvas_paleta = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_paleta.config(bg="black")
canvas_paleta.pack()
rec = canvas_paleta.create_rectangle(166, 65, 340, 220)
rec1 = canvas_paleta.create_rectangle(470, 55, 640, 200)
rec2 = canvas_paleta.create_rectangle(465, 270, 670, 440)
rec3 = canvas_paleta.create_rectangle(155, 290, 370, 430)
rec4 = canvas_paleta.create_rectangle(0, 0, 100, 550)
rec5 = canvas_paleta.create_rectangle(740, 0, 850, 550)
rec6 = canvas_paleta.create_rectangle(100, 535, 740, 550)
rec7 = canvas_paleta.create_rectangle(0, 0, 650, 10)


global ash, numero_imagen_ash, numero_imagen_pika
numero_imagen_ash = 0
numero_imagen_pika = 0


def movimiento_ash(movimientos, fondo):
    global numero_imagen_ash
    numero_imagen_ash += 1
    if numero_imagen_ash == len(movimientos):
        numero_imagen_ash = 0
    fondo.itemconfig(ash, image=movimientos[numero_imagen_ash])


def movimiento_pika(movimientos, fondo):
    global numero_imagen_pika
    numero_imagen_pika += 1
    if numero_imagen_pika == len(movimientos):
        numero_imagen_pika = 0
    fondo.itemconfig(pika, image=movimientos[numero_imagen_pika])


pueblo_paleta1 = load_img("paleta.png")
paleta = canvas_paleta.create_image(0, 0, anchor=NW, image=pueblo_paleta1)
pika1 = load_img("pikachu_e_1.png")
pika = canvas_paleta.create_image(370, 107, anchor=CENTER, image=pika1)
ash1 = load_img("ash_e_1.png")
ash = canvas_paleta.create_image(400, 100, anchor=CENTER, image=ash1)
casa_sprite1 = load_img("casa_sprite.png")
imagencasa = canvas_paleta.create_image(132, 6, anchor=NW, image=casa_sprite1)
imagencasa2 = canvas_paleta.create_image(435, -8, anchor=NW, image=casa_sprite1)
lab_sprite = load_img("lab_sprite.png")
lab_sprite1 = canvas_paleta.create_image(445, 228, anchor=NW, image=lab_sprite)
rec_bbox = canvas_paleta.bbox(rec)
rec1_bbox = canvas_paleta.bbox(rec1)
rec2_bbox = canvas_paleta.bbox(rec2)
rec3_bbox = canvas_paleta.bbox(rec3)
rec4_bbox = canvas_paleta.bbox(rec4)
rec5_bbox = canvas_paleta.bbox(rec5)
rec6_bbox = canvas_paleta.bbox(rec6)
rec7_bbox = canvas_paleta.bbox(rec7)

lista_objetos_paleta = [rec_bbox, rec1_bbox, rec2_bbox, rec3_bbox, rec4_bbox, rec5_bbox, rec6_bbox, rec7_bbox]

mover_arriba1, mover_abajo1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                                           load_img("ash_e_3.png"), ], \
                                                          [load_img("ash_f_1.png"), load_img("ash_f_2.png"),
                                                           load_img("ash_f_3.png")], \
                                                          [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                                           load_img("ash_d_3.png"), ], \
                                                          [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                                           load_img("ash_i_2.png")]

mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                           load_img("pikachu_e_3.png"), ], \
                                                          [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                           load_img("pikachu_f_3.png")], \
                                                          [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                           load_img("pikachu_d_3.png"), ], \
                                                          [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                           load_img("pikachu_i_2.png")]
#suave le voy a mandar foto a martin ok
def calcular_coords_x(coordenada_x1, coordenada_x2, lista_coordx):
    if coordenada_x1 > coordenada_x2:
        return lista_coordx
    else:
        lista_coordx.append(coordenada_x1)
        return calcular_coords_x(coordenada_x1 + 1, coordenada_x2, lista_coordx)


def calcular_coords_y(coordenada_y1, coordenada_y2, lista_coordy):
    if coordenada_y1 > coordenada_y2:
        return lista_coordy
    else:
        lista_coordy.append(coordenada_y1)
        return calcular_coords_y(coordenada_y1 + 1, coordenada_y2, lista_coordy)

def colisiones_arriba1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_paleta.coords(pika)[1] - 18 < lista_objetos[0][3]:
        if canvas_paleta.coords(pika)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_arriba1(lista_objetos[1:])
    else:
        return colisiones_arriba1(lista_objetos[1:])


def colisiones_abajo1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_paleta.coords(pika)[1] + 18 < lista_objetos[0][3]:
        if canvas_paleta.coords(pika)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_abajo1(lista_objetos[1:])
    else:
        return colisiones_abajo1(lista_objetos[1:])


def colisiones_derecha1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_paleta.coords(pika)[0] + 18 < lista_objetos[0][2]:
        if canvas_paleta.coords(pika)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_derecha1(lista_objetos[1:])
    else:
        return colisiones_derecha1(lista_objetos[1:])


def colisiones_izquierda1(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_paleta.coords(pika)[0] - 18 < lista_objetos[0][2]:
        if canvas_paleta.coords(pika)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_izquierda1(lista_objetos[1:])
    else:
        return colisiones_izquierda1(lista_objetos[1:])


def colisiones_arriba(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_paleta.coords(ash)[1] - 18 < lista_objetos[0][3]:
        if canvas_paleta.coords(ash)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_arriba(lista_objetos[1:])
    else:
        return colisiones_arriba(lista_objetos[1:])


def colisiones_abajo(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][1] <= canvas_paleta.coords(ash)[1] + 18 < lista_objetos[0][3]:
        if canvas_paleta.coords(ash)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
            return False
        else:
            return colisiones_abajo(lista_objetos[1:])
    else:
        return colisiones_abajo(lista_objetos[1:])


def colisiones_derecha(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_paleta.coords(ash)[0] + 18 < lista_objetos[0][2]:
        if canvas_paleta.coords(ash)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_derecha(lista_objetos[1:])
    else:
        return colisiones_derecha(lista_objetos[1:])


def colisiones_izquierda(lista_objetos):
    if len(lista_objetos) == 0:
        return True
    if lista_objetos[0][0] <= canvas_paleta.coords(ash)[0] - 18 < lista_objetos[0][2]:
        if canvas_paleta.coords(ash)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
            return False
        else:
            return colisiones_izquierda(lista_objetos[1:])
    else:
        return colisiones_izquierda(lista_objetos[1:])

def mover_arriba(event):
    if colisiones_arriba(lista_objetos_paleta) == False or colisiones_arriba1(lista_objetos_paleta) == False:
        movimiento_ash(mover_arriba1, canvas_paleta)
        canvas_paleta.move(ash, 0, 0)
        movimiento_pika(mover_arriba2, canvas_paleta)
        canvas_paleta.move(pika, 0, 0)
    else:
        movimiento_ash(mover_arriba1, canvas_paleta)
        canvas_paleta.move(ash, 0, -7)
        movimiento_pika(mover_arriba2, canvas_paleta)
        canvas_paleta.move(pika, 0, -7)


def mover_abajo(event):
    if colisiones_abajo(lista_objetos_paleta) == False or colisiones_abajo1(lista_objetos_paleta) == False:
        movimiento_ash(mover_abajo1, canvas_paleta)
        canvas_paleta.move(ash, 0, 0)
        movimiento_pika(mover_abajo2, canvas_paleta)
        canvas_paleta.move(pika, 0, 0)
    else:
        movimiento_ash(mover_abajo1, canvas_paleta)
        canvas_paleta.move(ash, 0, 7)
        movimiento_pika(mover_abajo2, canvas_paleta)
        canvas_paleta.move(pika, 0, 7)


def mover_derecha(event):
    if colisiones_derecha(lista_objetos_paleta) == False or colisiones_derecha1(lista_objetos_paleta) == False:
        movimiento_ash(mover_derecha1, canvas_paleta)
        canvas_paleta.move(ash, 0, 0)
        movimiento_pika(mover_derecha2, canvas_paleta)
        canvas_paleta.move(pika, 0, 0)
    else:
        movimiento_ash(mover_derecha1, canvas_paleta)
        canvas_paleta.move(ash, 7, 0)
        movimiento_pika(mover_derecha2, canvas_paleta)
        canvas_paleta.move(pika, 7, 0)


def mover_izquierda(event):
    if colisiones_izquierda(lista_objetos_paleta) == False or colisiones_izquierda1(lista_objetos_paleta) == False:
        movimiento_ash(mover_izq1, canvas_paleta)
        canvas_paleta.move(ash, 0, 0)
        movimiento_pika(mover_izq2, canvas_paleta)
        canvas_paleta.move(pika, 0, 0)
    else:
        movimiento_ash(mover_izq1, canvas_paleta)
        canvas_paleta.move(ash, -7, 0)
        movimiento_pika(mover_izq2, canvas_paleta)
        canvas_paleta.move(pika, -7, 0)


def lista_pokemon():
    ventana_pokemon = Tk()
    ventana_pokemon.title("lista de pokemon")
    ventana_pokemon.minsize(300, 300)
    ventana_pokemon.resizable(width=False, height=False)
    ventana_pokemon.config(bg='white')
    canvas_lista_pokemon = Canvas(ventana_pokemon, width=300, height=300, bd=0, highlightthickness=0)
    canvas_lista_pokemon.config(bg="black")
    canvas_lista_pokemon.pack()


def menu(event):
    ventana_menu = Tk()
    ventana_menu.title("Menu del jugador")
    ventana_menu.minsize(300, 300)
    ventana_menu.resizable(width=False, height=False)
    ventana_menu.config(bg='white')
    Button(ventana_menu, width=15, height=5, command=exit, text="Salir", bg="white", fg="black").place(x=25, y=200)
    Button(ventana_menu, width=15, height=5, command=exit, text="Guardar", bg="white", fg="black").place(x=25, y=100)
    Button(ventana_menu, width=15, height=5, command=lista_pokemon, text="Pokemon", bg="white", fg="black").place(x=25,
                                                                                                                  y=5)


ventana_principal.bind("<space>", menu)
ventana_principal.bind("<Up>", mover_arriba)
ventana_principal.bind("<Left>", mover_izquierda)
ventana_principal.bind("<Right>", mover_derecha)
ventana_principal.bind("<Down>", mover_abajo)

ventana_principal.update()
ventana_principal.mainloop()
